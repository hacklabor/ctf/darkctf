# Misc/Secret Of The Contract - 465 points
from Catamob, RDxR10

Ropsten network contains my dark secret. Help us find it.
Name of the contract was 0x6e5EA18371748Db7F12A70037d647cDFCf458e45

---
- The description hint to the 'Ropsten' network, which is the test network of Ethereum, so we head over to [etherscan.io](https://ropsten.etherscan.io/address/0x6e5ea18371748db7f12a70037d647cdfcf458e45), select the Ropsten test network and enter the provided address of the smart contract.
- Selecting the 'Contract' tab we can see the byte code of the smart contract. 
![](./contract.png)
Click 'Decompile ByteCode' gives us the following pseudo code:
```
def storage:
  unknown592f6623 is array of uint256 at storage 0
  message is array of uint256 at storage 1

def unknown592f6623(): # not payable
  return unknown592f6623[0 len unknown592f6623.length]

def message(): # not payable
  return message[0 len message.length]

#
#  Regular functions
#

def _fallback() payable: # default function
  revert

def update(string _newName) payable: 
  require calldata.size - 4 >= 32
  require _newName <= 4294967296
  require _newName + 36 <= calldata.size
  require _newName.length <= 4294967296 and _newName + _newName.length + 36 <= calldata.size
  message[] = Array(len=_newName.length, data=_newName[all])

```
- From the code we can learn, that the contract has two 'variables' to store data and a function called 'update' that can write a new value to the 'variable' message in storage slot 1. The `payable` keyword in the declaration of the 'update' function means this function can be invoked by paying a transaction to the contract.
- If if go to the 'Transactions' tab we can see the contract has 2 transactions:
![](./transactions.png)
- If we go open a single transaction and go to the 'State" tab, we can see the state transitions performed on the contract with this transaction.
	- 1. Contract creation: Sets storage 0 to `3772346e3534633731306e7d0`
	- 2. Invocation of update method: Sets storage 1 to `Hmm-6461726B4354467B3374683372333772346e3534633731306e7d0`
		- Overview for the second transaction which invokes the update method:
		![](./transaction_details.png)
- Now just concatenate the values of storage 1 and storage 2 and decode the hex: `darkCTF{3th3r3um_570r4g3_7r4n54c710n}`

## Team: Hacklabor; Time to solve appr. 1,0 h

## [hacklabor.de](hacklabor.de)
<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />
