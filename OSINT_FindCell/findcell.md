# OSINT/Find cell
from MrHappy,e11i0t



I lost my phone while I was travelling back to home, I was able to get back my eNB ID, MCC and MNC could you help me catch the tower it was last found.
note: decimal value upto 1 digit
Flag Format : darkCTF{latitude,longitude}

given file: challenge.txt

File content ```310 410 81907```

----


- MMC 310 + MNC 410 = at&t [https://en.wikipedia.org/wiki/Mobile_Network_Codes_in_ITU_region_3xx_(North_America)](https://en.wikipedia.org/wiki/Mobile_Network_Codes_in_ITU_region_3xx_(North_America))
- 81907 = eNB ID => Tower ID
- put in the data on [cellmapper.net](cellmapper.net) and you will get the geolocation

![flag.png](./pic/flag.png)[flag.png](./pic/flag.png)

----

# darkCTF{32.8,-24.5}

----

# Thanks a lot MrHappy,e11i0t for the interesting challenge

## Team: Hacklabor; Time to solve appr. 1,5 h

## [hacklabor.de](hacklabor.de)
<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />
