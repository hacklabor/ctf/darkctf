# Forensics/AW

from e11i0t

"``Hello, hello, Can you hear me, as I scream your Flag!`` "

[Here Downloadlink]

---

I downloaded the "Spectre.mp4" and dropped it in "``Sonic Visualiser``". Then I added a Spectrogram (which channel or all is wayne). At time 1:10 between 5000 Hz and 7000 Hz I saw this:

![aw0.jpg](./pic/aw0.jpg)

I zoomed in played a bit with the gain for better contrast and got the flag:

![aw.png](./pic/aw.png)

whole flag is:

darkCTF{1_l0v3_5p3ctr3_fr0m_4l4n}

----

# Thanks a lot e11i0t we had much fun with this challenge

## Team: Hacklabor; Time to solve appr. 5 min

## [hacklabor.de](hacklabor.de)
<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />


