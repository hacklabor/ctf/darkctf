# Forensics/Wolfie's Contact

from Wolfy

"``Wolfie is doing some illegal work with his friends find his contacts.``"

[Here Downloadlink]

---

I downloaded the "wolfie_evidence .rar", unziped it and got the "wolfie_evidence.E01"-file. This is an special Disc-Image-Format wich can be open with programms like "``Autopsy``".

![autopsy_contact.jpg](./pic/autopsy_contact.jpg)

In the folderstructure I found the folder contacs which has some contact-files.
I took a closer look and found the 4 parts of the flag in:
- dealer.contact
- Money Giver.contact
- broker.contact
- target.contact

![dealer.jpg](./pic/dealer.jpg)

whole flag is:

darkCTF{C0ntacts_4re_1mp0rtant}

----

# Thanks a lot wolfie we had much fun with this challenge

## Team: Hacklabor; Time to solve appr. 30 min

## [hacklabor.de](hacklabor.de)
<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />



