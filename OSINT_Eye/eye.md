# OSINT/Eye (454 Pkt.)
from MrHappy

Can you find the location of this image? Precision up to 3 decimal points.

FlagFormat: darkCTF{latitude,longitude} 

later from discord
FlagFormat: darkCTF{xx.xxx,yy.yyy}

given file
![eye.jpg](./Pic/eye.jpg)[eye.jpg](./Pic/eye.jpg)

----

# Analysis

- a picture search on [https://yandex.com/images/](https://yandex.com/images/) offers us a simular picture from an india Travel Forum 
![simular.png](./Pic/simular.png)[simular.png](./Pic/simular.png)
![ooty2.png](./Pic/ooty2.png)[ooty2.png](./Pic/ooty2.png)
- now we know the place :) is in [udagamandalam-ooty_india](https://www.indiamike.com/india/udagamandalam-ooty-f147/ooty-t43957/#post384188)
- when we zoom in on the simular picture we find the "Chocolate Factory" ![chocolatefactory.png](./Pic/chocolatefactory.png) [chocolatefactory.png](./Pic/chocolatefactory.png)
- Google Image search = chocolate factory ooty -->![CFFB.png](./Pic/CFFB.png)[CFFB.png](./Pic/CFFB.png)
- its the same Shop. The date of the "simular picture" and the open date are just a few days difference
- now we have a name and an address ![compare.png](./Pic/compare.png)[compare.png](./Pic/compare.png)
- Google Maps search = Suji's-New-Chocolate-Factory-Ooty -->[https://www.google.com/maps/place/New+chocolate+Factory+Outlet/@11.4159395,76.7143163,1216m/data=!3m1!1e3!4m5!3m4!1s0x3ba8bd114f7c8e4b:0xfdec3eea76f84153!8m2!3d11.4171776!4d76.7158724](https://www.google.com/maps/place/New+chocolate+Factory+Outlet/@11.4159395,76.7143163,1216m/data=!3m1!1e3!4m5!3m4!1s0x3ba8bd114f7c8e4b:0xfdec3eea76f84153!8m2!3d11.4171776!4d76.7158724)
![finalarea.png](./Pic/finalarea.png)[finalarea.png](./Pic/finalarea.png)
- red = Warehouse, green = 4 significant roofes, purple = the posible place wehre the picture is taken
- in the Google Streetview sphere [Streetview](https://www.google.com/maps/@11.412075,76.7204666,3a,75y,334.93h,86.27t/data=!3m8!1e1!3m6!1sAF1QipNDgTPdRG5T82tKGbLIUAIlAnZKGoVTca5nXt1Z!2e10!3e11!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNDgTPdRG5T82tKGbLIUAIlAnZKGoVTca5nXt1Z%3Dw203-h100-k-no-pi0-ya46.581795-ro-0-fo100!7i6912!8i3456) we see clear the fance like in the given picture ![fence.png](./Pic/fence.png)[fence.png](./Pic/fence.png)
- we are at the right place but we dont get the flag (11.412,76.720) =/
- sorry then we had to do a little brute force and playing with the last digit +-1 
- I dont know any more if it was darkCTF{11.411,76.720} or darkCTF{11.413,76.720} . 100m +- 

That was a little frustrating, after a 7200km long intresting, educational and exciting trip to india. on the other hand Great Challenge!

----

# Thanks a lot MrHappy we had mutch fun with this challenge

## Team: Hacklabor; Time to solve appr. 2 h

## [hacklabor.de](hacklabor.de)
<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />

