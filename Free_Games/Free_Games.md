# Forensics/Free Games

from Wolfy

"`` Wolfie getting free games from somewhere. Find the full url to that game. Note: Use the same file provided in Wolfie's Contacts Flag Format: darkCTF{http://site} `` "

[Here Downloadlink]

---

As it is said in the discription, I used the same EnCase-file like in the "Wolfie's Contact" - Challenge and opened it in "``Autopsy``". I lookt at the flagformat and saw that it begins with "http://" and thought that there will be not much URL's with it. So I just searched the string (keyword) "http://" in "``Autopsy``". I got 43 Results and one of it was "``Web Downloads Artifact``" and in there i found the flag:


![free.jpg](./pic/free.jpg)



whole flag is:

darkCTF{http://aries.dccircle34.com/realitydownloadgo/c4d37739ca3dc3ed2d4852395d5ed228/784b4647446e334c58556e5473326556422e624f612e51432e4a6472/2019/07/31/PencakSilat2_1.zip}

## Note:
1. I knew that this is a game because looked at nearly all files for another challenge and found the EXE-File in "``downlaods``" and looked it up.
2. Autopsy is configured to search for common web artifacts from today's major browsers, including: 
    - Firefox
    - Chrome
    - Internet Explorer
3. Autopsy do this automatically when you open the EnCase-file.
4. I could had just take a look at the "``PencakSilat2_1.zip:Zone.Identifier``" - File which is also in "``downlaods``". It's an ADS (altaernate data stream) caused by the Internet Explorer which stores Informations about downloads from the internet which can be dangerous. The ":Zone.Identifier" tells the "Windows Explorer also, that it's from the internet.

----

# Thanks a lot Wolfy we had much fun with this challenge

## Team: Hacklabor; Time to solve appr. 5 min

## [hacklabor.de](hacklabor.de)
<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />


